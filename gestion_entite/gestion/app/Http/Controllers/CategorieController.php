<?php

namespace App\Http\Controllers;

use App\Models\Categorie;
use App\Models\Entite;
use Illuminate\Http\Request;

class CategorieController extends Controller
{
    public function index($categorie)
    {
        $categorie = Categorie::where('nom',$categorie)->first();
        $entites = Entite::where("categorie_id", $categorie->id);
        $entites = $entites->simplePaginate(10);

        return view("entite.indexCat", compact("entites"));

    }

}
