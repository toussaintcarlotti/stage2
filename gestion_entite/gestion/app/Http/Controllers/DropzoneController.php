<?php

namespace App\Http\Controllers;

use App\Models\Entite;
use App\Models\Image;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as MakeImage;

class DropzoneController extends Controller
{
    function index($entiteId)
    {

        return view('dropzone',compact('entiteId'));
    }

    function upload()
    {
        $entite = Entite::find(\request('entiteId'));
        $image = request()->file('file');
        $path = $image->hashName('entites/');
        if (!file_exists(storage_path('app/public/entites/'))){
            mkdir(storage_path('app/public/entites/'));
        }
        MakeImage::make($image)
            ->resize(1200, null, function ($constraint)
            {$constraint->aspectRatio();})
            ->crop(1200,560)
            ->save(storage_path('app/public/'.$path));
        $entite->images()->save(Image::create([
            'lien'=> 'storage/'.$path,
            'type'=>'principal'
        ]));

        return response()->json(['success' => $path]);
    }

    function fetch()
    {
        $entite = Entite::find(\request('entiteId'));
        $images = \File::allFiles(storage_path('app/public/'));
        $output = '<div class="row">';
        foreach($entite->images as $image)
        {

            $output .= '
      <div class="col-md-2" style="margin-bottom:16px;" align="center">
                <img src="'.url( $image->lien).'" class="img-thumbnail" width="175" height="175" style="height:175px;" />
                <button type="button" class="btn btn-link remove_image" id="'.$image->lien.'">Remove</button>
            </div>
      ';
        }
        $output .= '</div>';
        echo $output;
    }

    function delete()
    {
        if(request('name'))
        {
            Image::where('lien', request('name'))->delete();
            \File::delete(request('name'));

        }
    }
}
