<?php

namespace App\Http\Controllers;

use App\Models\Categorie;
use App\Models\Entite;
use App\Models\Image;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Storage;


class EntiteController extends Controller
{
    public function index()
    {
        $entites = new Entite;

        if (request('nom')) {
            $entites = $entites->where('nom','like',request('nom').'%');
        }
        if (request('ville')) {
            $entites = $entites->where('ville','like',request('ville').'%');
        }

        if (request('categorie') ){
            $entites = $entites->where("categorie_id", request('categorie'));
        }
        $entites = $entites->simplePaginate(10);
        $cats = Categorie::get();

        return view("entite.index", compact("entites","cats"));

    }

    public function create()
    {
        $categories = Categorie::get();
        return view('entite.create',compact('categories'));
    }

    public function store()
    {
        $verif = 0;
        $attribute = request()->validate([
            'nom' => 'required|max:255|unique:entites',
            'email' => 'required|email|max:255',
            'tel' => 'required',
            'description' =>'sometimes|nullable',
            'ville' => 'required',
            'codePostal' => 'required',
            'adresse' => 'required',
            'latitude' => 'sometimes',
            'longitude' => 'sometimes'
        ]);
        request()->validate([
            'image' => 'sometimes|nullable|image',
            'categorie' =>'required'
        ]);


        $entite = auth()->user()->entites()->save(Entite::create($attribute));
        Categorie::find(request('categorie'))->entites()->save($entite);


        if(request()->has('image')) {
            $entite->addPhotoPrincipal(request()->file('image'));
        }

        return redirect()->to('/');
    }

    public function show($id)
    {
        $entite = Entite::find($id);
        return view('entite.show',compact('entite'));
    }


    public function edit($id)
    {
        $entite = Entite::find($id);
        $categories = Categorie::get();
        return view('entite.edit', compact('entite','categories'));
    }

    public function update($id)
    {

        $entite = Entite::find($id);
        $nom = $entite->nom;
        $attribute = request()->validate([
            'email' => 'required|email|max:255',
            'tel' => 'required',
            'description' =>'sometimes',
            'ville' => 'required',
            'codePostal' => 'required',
            'adresse' => 'required',
            'latitude' => 'sometimes',
            'longitude' => 'sometimes'

        ]);
        request()->validate([
            'image' => 'sometimes|nullable|image',
            'categorie_id' =>'required'
        ]);

        if ($nom != request('nom')){
            $attribute = request()->validate([
                'nom' => 'required|max:255|unique:entites'
            ]);
        }
        if(request()->has('image')) {
            if ($entite->images()->where('type','principal')->first()){
                $entite->editPhotoPrincipal(request()->file('image'));
            }
            else{
                $entite->addPhotoPrincipal(request()->file('image'));
            }
        }
        Categorie::find(request('categorie_id'))->entites()->save($entite);

        $entite->update($attribute);
        return redirect(RouteServiceProvider::HOME);

    }

    public function destroy(Entite $entite){

        foreach ($entite->images as $image){
            Storage::disk('public')->delete($image->lien);
            $image->delete();
        }

        $entite->delete();

        return redirect('user/'.auth()->user()->id.'/entites');
    }
}
