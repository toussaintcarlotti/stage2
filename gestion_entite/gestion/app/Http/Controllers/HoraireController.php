<?php

namespace App\Http\Controllers;

use App\Models\Categorie;
use App\Models\Entite;
use App\Models\Horaire;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class HoraireController extends Controller
{
    public function create($entiteId)
    {

        return view('horaire.createHoraire',compact('entiteId'));
    }

    public function store($entiteId)
    {

        $tab = ['lundi','mardi','mercredi','jeudi','vendredi','samedi','dimanche'];



        foreach ($tab as $element){
                request()->validate([
                    $element => 'required',
                    $element.'1' => Rule::requiredIf(request('2')),
                    $element.'2' => Rule::requiredIf(request('1')),
                    $element.'3' => Rule::requiredIf(request('1') || request('4')),
                    $element.'4' => Rule::requiredIf(request('1') || request('3'))
                ]);
                if (request($element.'1')){
                    $attributes = [
                        'days' => request($element),
                        'h1' => request($element.'1'),
                        'h2' =>  request($element.'2'),
                        'h3' =>  request($element.'3'),
                        'h4' => request($element.'4')
                    ];
                    Entite::find($entiteId)->horaires()->save(Horaire::create($attributes));
                }
        }
        return redirect('entites/edit/'.$entiteId);
    }
    public function edit(Entite $entite)
    {
        return view('horaire.editHoraire',compact('entite'));
    }

    public function update(Entite $entite)
    {
        $tab = ['lundi','mardi','mercredi','jeudi','vendredi','samedi','dimanche'];


        foreach ($tab as $element){

            request()->validate([
                $element => 'required',
                $element.'1' => Rule::requiredIf(request($element.'2') || request($element.'3') || request($element.'4')),
                $element.'2' => Rule::requiredIf(request($element.'1') || request($element.'3') || request($element.'4')),
                $element.'3' => Rule::requiredIf((request($element.'1') && \request($element.'4')) || request($element.'4')),
                $element.'4' => Rule::requiredIf((request($element.'1') && \request($element.'3')) || request($element.'3'))
            ]);
            if (request($element.'1')){
                $attributes = [
                    'days' => request($element),
                    'h1' => request($element.'1'),
                    'h2' =>  request($element.'2'),
                    'h3' =>  request($element.'3'),
                    'h4' => request($element.'4')
                ];
                if ($entite->horaires()->where('days',$element)->first()){
                    $horaire = Horaire::find($entite->horaires()->where('days',$element)->first()->id)->where('days',$element)->update($attributes);
                }else{
                    Entite::find($entite->id)->horaires()->save(Horaire::create($attributes));
                }
            }
        }


        return redirect('entites/edit/'.$entite->id);
    }
    public function destroy(Entite $entite){

        foreach ($entite->horaires as $horaire){
            $horaire->delete();
        }
        return redirect('entites/edit/'.$entite->id);
    }

}
