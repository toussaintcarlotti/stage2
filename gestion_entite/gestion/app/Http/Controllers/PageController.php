<?php

namespace App\Http\Controllers;

use App\Models\Categorie;
use App\Models\Entite;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index()
    {
        $entites = Entite::get();
        return view("index",compact('entites'));

    }

}
