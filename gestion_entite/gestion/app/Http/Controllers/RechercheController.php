<?php

namespace App\Http\Controllers;

use App\Models\Categorie;
use App\Models\Entite;
use Illuminate\Http\Request;
use function Symfony\Component\String\s;

class RechercheController extends Controller
{
    public function index()
    {
        $entites = new Entite;
        $search = null;
        if (request('nom')) {
            $search = \request('nom');
            $entites = $entites->where('nom','like',request('nom').'%');
        }
        if (request('ville')) {
            $entites = $entites->where('ville','like',request('ville').'%');
        }

        if (request('categorie') ){
            $entites = $entites->where("categorie_id", request('categorie'));
        }
        $entites = $entites->simplePaginate(10);
        $cats = Categorie::get();

        return view("entite.rechercheEntite", compact("entites","cats","search"));

    }
}
