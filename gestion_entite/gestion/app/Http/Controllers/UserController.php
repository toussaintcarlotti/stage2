<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function create()
    {
        return view('user.create');
    }
    public function store()
    {

        $attribute = request()->validate([
            'nom' => 'required|string|max:255',
            'prenom' => 'required|string',
            'civilite' => 'required|string',
            'numero' => 'required|numeric',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|confirmed|min:8'
        ]);
        $attribute['password'] = Hash::make(request('password'));

        Auth::login($user = User::create($attribute));
        return redirect()->to('entites/create');
    }

    public function edit()
    {
        return view('user.edit');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update()
    {

        $user = auth::user();
        $email = $user->email;

        $attribute = request()->validate([
            'nom' => 'required|string|max:255',
            'prenom' => 'required|string',
            'civilite' => 'required|string',
            'numero' => 'required|numeric',
            'password' => 'required|string|min:8'
        ]);

        if ($email != request('email')){
            $attribute = request()->validate([
                'email' => 'required|string|email|max:255|unique:users'
            ]);
        }

        $attribute['password'] = Hash::make($attribute['password']);
        $user->update($attribute);


        return redirect(RouteServiceProvider::HOME);
    }
}
