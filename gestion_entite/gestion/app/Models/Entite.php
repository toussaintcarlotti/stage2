<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as MakeImage;
/**
 * @method static find($id)
 */
class Entite extends Model
{

    protected $fillable = [
        'nom',
        'email',
        'tel',
        'ville',
        'codePostal',
        'adresse',
        'latitude',
        'longitude',
        'description',
        'image'
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function categorie()
    {
        return $this->belongsTo(Categorie::class);
    }

    public function images()
    {
        return $this->hasMany(Image::class);
    }

    public function horaires()
    {
        return $this->hasMany(Horaire::class);
    }

    public function addPhotoPrincipal($photo)
    {
        $path = $photo->hashName('entites');
        MakeImage::make($photo)->resize(1200)->crop(1200,560)->save(storage_path('app/public/'.$path));
        $this->images()->save(Image::create([
            'lien'=> 'storage/'.$path,
            'type'=>'principal'
            ]));

    }


}
