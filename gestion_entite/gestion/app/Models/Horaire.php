<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Horaire extends Model
{
    use HasFactory;
    protected $fillable = [
        'days',
        'h1',
        'h2',
        'h3',
        'h4'
    ];

    public function entite()
    {
        return $this->belongsTo(Entite::class);
    }
    public $timestamps = false;
}
