<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;
    protected $fillable = [
        'lien',
        'type'
    ];

    public function entite()
    {
        return $this->belongsTo(Entite::class);
    }
}
