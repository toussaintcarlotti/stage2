<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entites', function (Blueprint $table) {
            $table->id();
            $table->string("nom");
            $table->string("email");
            $table->integer("tel");
            $table->string("ville")->nullable();
            $table->unsignedBigInteger("codePostal")->nullable();
            $table->string("adresse")->nullable();
            $table->float("latitude",10,6)->nullable();
            $table->float("longitude",10,6)->nullable();
            $table->longText("description")->nullable();
            $table->unsignedBigInteger("user_id")->nullable()->index();
            $table->unsignedBigInteger("categorie_id")->nullable()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entites');
    }
}
