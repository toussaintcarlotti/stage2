<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="{{ url('/css/style.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ url('/css/dropDown.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ url('/css/navBar.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ url('/css/sideBar.css') }}" />
    <!--  bootstrap  -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

    <!--  Flatpickr  -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <!--  google  -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDOk5qhb0wY9-36eo1IgzYd4dZXEMs-xDs&libraries=places&callback=initAutocomplete" async defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
    <!--  algolia  -->
    <script src="https://cdn.jsdelivr.net/npm/places.js@1.19.0"></script>
    <title>GestionEntite</title>

</head>
<body>

    <nav class="navbar navbar-expand-lg fixed-top" style="margin-bottom: 20px;">

            <a class="navbar-brand" href="/"><img class="logo" src="{{ url('image/logo.png') }}" width="50px" ></a>
        <button class="navbar-toggler" onclick="navBlack()" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <svg xmlns="http://www.w3.org/2000/svg" width="25" fill="black" class="bi bi-list" viewBox="0 0 16 16">
                <path fill-rule="evenodd" d="M2.5 11.5A.5.5 0 0 1 3 11h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4A.5.5 0 0 1 3 7h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4A.5.5 0 0 1 3 3h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"/>
            </svg>
        </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto text-center">
                    <li class="dropdown nav-item " >
                        @if( auth()->check() )
                            <button type="button" class="dropdown__title" aria-expanded="false" aria-controls="sweets-dropdown">
                                Salut {{ auth()->user()->prenom }}
                            </button>
                            <ul class="dropdown__menu"  id="sweets-dropdown">
                                <li class="nav-item">
                                    <a  class="nav-link titre" aria-current="page" href="{{ url('user/'.auth()->user()->id.'/entites') }}">Mes entites</a>
                                </li>
                                <li class="nav-item" >
                                    <a  class="nav-link " href="{{ url('user/edit') }}">Mon compte</a>
                                </li>
                                <a class="nav-link">
                                    <li class="nav-item form-inline">

                                        <form class="d-flex" method="POST" action="{{ url('logout') }}" >
                                            @csrf

                                            <button class="btn" style="color: rgba(255,0,0,0.54);border: none;background: none" type="submit">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-box-arrow-right" viewBox="0 0 16 16">
                                                    <path fill-rule="evenodd" d="M10 12.5a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v2a.5.5 0 0 0 1 0v-2A1.5 1.5 0 0 0 9.5 2h-8A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-2a.5.5 0 0 0-1 0v2z"/>
                                                    <path fill-rule="evenodd" d="M15.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 0 0-.708.708L14.293 7.5H5.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3z"/>
                                                </svg>
                                                Se déconnecter</button>

                                        </form>

                                    </li>
                                </a>
                            </ul>
                        @else
                            <button type="button" class="dropdown__title " aria-expanded="false" aria-controls="sweets-dropdown">
                                Professionnel ?
                            </button>
                            <ul class="dropdown__menu" id="sweets-dropdown">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('login') }}">Log In</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('registration')}}">Register</a>
                                </li>
                            </ul>
                        @endif
                    </li>
                </ul>

                <ul class="navbar-nav">
                    @if( auth()->check() )
                    @endif
                        <li class="nav-item ">
                           <a class="nav-link" href="{{ url('entites/index') }}" style="width: 130px;padding-top: 15px">
                               Voir tout
                           </a>
                        </li>
                </ul>

                <ul class="navbar-nav ml-auto">
                    <li class="nav-item" style="max-width: 280px;text-align: center;margin: auto">
                        <form class="form-inline" action="{{ url('entites/recherche') }}" method="post">
                            @csrf
                            <input type="search" placeholder="Rechercher" class="form-control mr-1" name="nom">
                            <button class="btn btn-primary" type="submit" style="border:transparent;background: transparent">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20"  fill="black" class="bi bi-search" viewBox="0 0 16 16">
                                    <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                                </svg>
                            </button>

                        </form>
                    </li>
                </ul>
            </div>
    </nav>

<div style="margin-top: 100px;">
    @yield('contenu')
</div>


</body>


<script >
    $(document).ready(function() {
        $(".menu-icon").on("click", function() {
            $("nav ul").toggleClass("showing");
        });
    });

    // Scrolling Effect

    $(window).on("scroll", function() {
        if($(window).scrollTop()) {
            $('nav').addClass('black');
        }

        else{
            if (! $("#navbarSupportedContent").is(":visible") ) {
                $('nav').removeClass('black');
            }
            if($(window).width() > 800){
                $('nav').removeClass('black');
            }
        }
    })
    function navBlack(){

        if(! $(window).scrollTop()) {

            if($('nav').hasClass('black')) {
                $('nav').removeClass('black');
            }else{
                $('nav').addClass('black');
            }
        }
    }
</script>
</html>
