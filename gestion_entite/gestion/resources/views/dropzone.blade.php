@extends('base')

@section('contenu')
    <div class="container-fluid" style="max-width: 1350px">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.js"></script>
        <div class="container-fluid">
            <br />
            <h3 align="center">Image Upload in Laravel using Dropzone</h3>
            <br />

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Select Image</h3>
                </div>
                <div class="panel-body">
                    <form id="dropzoneForm" class="dropzone" action="{{ route('dropzone.upload') }}">
                        @csrf
                        <input type="hidden" value="{{ $entiteId }}" name="entiteId" id="entiteId">
                    </form>
                    <div align="center">
                        <button type="button" class="btn btn-info" id="submit-all">Upload</button>
                    </div>
                </div>
            </div>
            <br />
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Uploaded Image</h3>
                </div>
                <div class="panel-body" id="uploaded_image">

                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        Dropzone.options.dropzoneForm = {
            autoProcessQueue : false,
            acceptedFiles : ".png,.jpg,.gif,.bmp,.jpeg",

            init:function(){
                var submitButton = document.querySelector("#submit-all");
                myDropzone = this;

                submitButton.addEventListener('click', function(){
                    myDropzone.processQueue();
                });

                this.on("complete", function(){
                    if(this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0)
                    {
                        var _this = this;
                        _this.removeAllFiles();
                    }
                    load_images();
                });

            }

        };

        load_images();

        function load_images()
        {
            var entiteId = $('#entiteId').val();

            $.ajax({
                url:"{{ route('dropzone.fetch') }}",
                data:{
                    entiteId : entiteId
                },
                success:function(data)
                {
                    $('#uploaded_image').html(data);
                }
            })
        }

        $(document).on('click', '.remove_image', function(){
            var name = $(this).attr('id');
            var entiteId = $('#entiteId').attr('id');
            $.ajax({
                url:"{{ route('dropzone.delete') }}",
                data:{
                    name : name,
                    entiteId : entiteId
                },
                success:function(data){
                    load_images();
                }
            })
        });
    </script>

@endsection
