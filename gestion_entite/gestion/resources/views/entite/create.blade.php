@extends('base')

@section('contenu')
<style>
    img{
        max-height: 300px;
        border-radius: 20px;
    }
    input[type=file]{
        display: none;
    }

    .label-file:hover {
        color: #25a5c4;
    }

</style>





<div class="container-fluid" style="max-width: 1350px">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form method="POST" action="{{ url('entites/create') }}" enctype="multipart/form-data" >
        @csrf

        <div class="row d-flex justify-content-around" >

            <div class="col-md-5 text-center" >
                <label for="image" class="label-file ">
                    image<br/>
                    <svg xmlns="http://www.w3.org/2000/svg" width="30"  fill="currentColor" class="bi bi-plus-circle" viewBox="0 0 16 16">
                        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                        <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                    </svg>
                </label>
                <input type="file" name="image" id="image" onchange="previewFile(this);" >
                <img id="previewImg" style=" max-width: 100%;
        height: auto;max-height: 300px;border-radius: 20px" >

            </div>



        </div>
        <div class="row d-flex justify-content-around" style="margin-top: 5% ">
            <div class="col-md-12 p-2" style="border: 3px solid rgba(229,229,229,0.47);border-radius: 15px">
                <div  style="text-align: justify;">
                    <input  placeholder="Nom" type="text" style="border: 0px;font-weight: bold;color: black;font-size: 30px" class="form-control mt-4 mb-3" id="nom" name="nom" >
                    <textarea  placeholder="Description"   style="min-height:100px; height:auto;border:0px;height: 80%;color: black;" class="form-control mt-4 mb-3" id="description" name="description"></textarea>
                </div>
            </div>

        </div>
        <div class="row d-flex justify-content-around" style="margin-top: 5% ">
            <div class="col-md-2 text-center"  >
                @if($categories->first() )
                    <div class="form-group">
                        <label for="categorie">Categorie</label>
                        <select class="form-control" id="categorie" name="categorie">
                            @foreach($categories as $categorie)
                                <option value="{{ $categorie->id }}">{{ $categorie->nom }}</option>
                            @endforeach
                        </select>
                    </div>
                @endif
            </div>
            <div class="col-md-3 p-2" style="min-width: 300px">
                <div class="boite mb-4">
                    <h5 style="text-align: center">Coordonnée</h5>
                    <div class="card-body">
                        <input style="border: 0px;color: black;background: transparent" placeholder="Email" type="email" class="form-control" id="email" name="email" autocomplete="email"><br/>

                        <input style="border: 0px;color: black;background: transparent" placeholder="Numero"  type="text" class="form-control" id="numero" name="tel" autocomplete="tel">

                    </div>
                </div >
            </div>
                <div class="col-md-3 p-2" style="min-width: 300px;">
                    <div class="boite mb-4">
                        <h5 style="text-align: center">Localisation</h5>
                        <div class="card-body">
                            <input placeholder="Adresse" style="border: 0px;color: black;background: transparent" type="search" name="adresse" class="form-control" id="form-address"  />
                            <input placeholder="Ville" style="border: 0px ;color: black;background: transparent" type="text" name="ville" class="form-control" id="form-city" >
                            <input placeholder="Code postal" style="border: 0px;color: black;background: transparent" type="text" class="form-control" name="codePostal" id="form-zip" >
                            <input style="border: 0px;color: black;background: transparent" type="hidden" name="latitude" id="latitude" >
                            <input style="border: 0px;color: black;background: transparent" type="hidden" name="longitude" id="longitude" >

                        </div>
                    </div >
                </div>

        </div>
        <div class="text-center mb-3">
            <button type="submit" class="btn btn-primary mt-6">Créer</button>
        </div>
    </form>

</div>
<script type="text/javascript" >
    (function() {
        var placesAutocomplete = places({
            appId: 'plYDAJ75QJ1U',
            apiKey: 'db4d061d4ca7bc1db029881082af4081',
            container: document.querySelector('#form-address'),
            templates: {
                value: function(suggestion) {
                    return suggestion.name;
                }
            }
        }).configure({
            type: 'address'
        });
        placesAutocomplete.on('change', function resultSelected(e) {

            document.querySelector('#form-city').value = e.suggestion.city ;
            document.querySelector('#form-zip').value = e.suggestion.postcode ;
            var coord = e.suggestion.latlng ;
            if(coord){
                document.querySelector('#latitude').value = coord.lat ;
                document.querySelector('#longitude').value = coord.lng ;
            }
        });
    })();

    function previewFile(input){
        var file = $("input[type=file]").get(0).files[0];
        if(file){
            var reader = new FileReader();
            reader.onload = function(){
                $("#previewImg").attr("src", reader.result);
            }
            reader.readAsDataURL(file);
        }
    }

    var textarea = document.querySelector('textarea');
    textarea.addEventListener('keydown', autosize);

    function autosize(){
        var el = this;
        setTimeout(function(){
            el.style.cssText = 'height:auto; padding:0';
            // for box-sizing other than "content-box" use:
            // el.style.cssText = '-moz-box-sizing:content-box';
            el.style.cssText = 'height:' + el.scrollHeight +  'px';
        },0);
    }

</script>

@endsection
