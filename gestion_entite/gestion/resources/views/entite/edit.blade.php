@extends('base')

@section('contenu')
    <style>
        img{
            max-height: 300px;
            border-radius: 20px;
        }
        input[type=file]{
            display: none;
        }

        .label-file:hover {
            color: #25a5c4;
        }

    </style>

    <form action="{{ url("entites/delete/".$entite->id) }}" method="POST" style="text-align: right;margin: 10px">
        @csrf
        @method("delete")
        <button class="btn btn-danger" type="submit" >
            Supprimer l'entite
        </button>
    </form>

    <div class="container-fluid" style="max-width: 1350px">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif


        <form method="POST" action="{{ url('entites/edit/'.$entite->id) }}" enctype="multipart/form-data" >

            @csrf

            <div class="row d-flex justify-content-around" >
                <div class="col-md-5 text-center" >

                    <img id="previewImg" style=" max-width: 100%;
        height: auto;max-height: 300px;border-radius: 20px" @if($entite->images()->where('type','principal')->first())src="{{ url($entite->images()->where('type','principal')->first()->lien) }}" @endif >
                    <br/>
                    <a href="{{ url('dropzone/index/'.$entite->id) }}">
                    <label for="image" class="label-file ">
                        image<br/>
                        <svg xmlns="http://www.w3.org/2000/svg" width="30"  fill="currentColor" class="bi bi-plus-circle" viewBox="0 0 16 16">
                            <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                            <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                        </svg>
                    </label>
                    </a>
                </div>
                <div class="col-md-7 p-2">
                    <div  style="text-align: justify;">
                        <input  placeholder="Nom" type="text" style="border: 0px;font-weight: bold;color: black;font-size: 30px" value="{{ $entite->nom }}" class="form-control mt-4 mb-3" id="nom" name="nom" >
                        <textarea  placeholder="Description"   style="min-height:100px; height:auto;border:0px;height: 80%;color: black;" class="form-control mt-4 mb-3" id="description" name="description">{{ $entite->description }}</textarea>
                    </div>
                </div>



            </div>

            <div class="row d-flex justify-content-around" style="margin-top: 5% ">
                @if($categories->first())
                    <div class="col-md-2 text-center"  >
                        <div class="form-group">
                            <label for="categorie_id">Categorie</label>
                            <select class="form-control" id="categorie_id" name="categorie_id">
                                @foreach($categories as $categorie)
                                    <option @if($entite->categorie->id == $categorie->id) selected @endif value="{{ $categorie->id }}">{{ $categorie->nom }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                @endif
                <div class="col-md-3 p-2" style="min-width: 300px">
                    <div class="boite mb-4">
                        <h5 style="text-align: center">Coordonnée</h5>
                        <div class="card-body">
                            <input style="border: 0px;color: black;background: transparent" value="{{ $entite->email }}" placeholder="Email" type="email" class="form-control" id="email" name="email" autocomplete="email"><br/>

                            <input style="border: 0px;color: black;background: transparent" value="{{ $entite->tel }}" placeholder="Numero"  type="text" class="form-control" id="numero" name="tel" autocomplete="tel">
                        </div>
                    </div >
                </div>
                <div class="col-md-4 p-2" style="min-width: 300px;">
                    <div class="boite mb-4 text-center">
                        <h5 style="text-align: center">Horaire</h5>
                        <div class="card-body">
                            @if($entite->horaires->first())
                                <a href="{{ url('horaires/edit/'.$entite->id) }}" style="color: black" id="horaire">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="30" fill="black" class="bi bi-pencil-square" viewBox="0 0 16 16">
                                        <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                        <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                    </svg>
                                </a>
                            @else
                                <a id="horaire" for="horaire" href="{{ url('horaires/create/'.$entite->id) }}" style="color: black">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="30" fill="black" class="bi bi-plus-circle" viewBox="0 0 16 16">
                                        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                        <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                                    </svg>
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
                @if($entite->ville || $entite->adresse || $entite->codePostal)
                    <div class="col-md-3 p-2" style="min-width: 300px;">
                        <div class="boite mb-4">
                            <h5 style="text-align: center">Localisation</h5>
                            <div class="card-body">
                                <input style="border: 0px;color: black;background: transparent" @if($entite->adresse) value="{{ $entite->adresse }}" @endif type="search" name="adresse" class="form-control" id="form-address"  />
                                <input style="border: 0px;color: black;background: transparent" @if($entite->ville) value="{{ $entite->ville }}" @endif type="text" name="ville" class="form-control" id="form-city" >
                                <input style="border: 0px;color: black;background: transparent" @if($entite->codePostal) value="{{ $entite->codePostal }}" @endif type="text" class="form-control" name="codePostal" id="form-zip" >
                                <input style="border: 0px;color: black;background: transparent" @if($entite->latitude) value="{{ $entite->latitude }}" @endif type="hidden" name="latitude" id="latitude" >
                                <input style="border: 0px;color: black;background: transparent" @if($entite->longitude) value="{{ $entite->longitude }}" @endif type="hidden" name="longitude" id="longitude" >
                            </div>
                        </div >
                    </div>
                @endif
            </div>
            <div class="text-center mb-3">
                <button type="submit" class="btn btn-primary mt-6">Valider</button>
            </div>
        </form>

    </div>



    <script type="text/javascript" >

        (function() {
            var placesAutocomplete = places({
                appId: 'plYDAJ75QJ1U',
                apiKey: 'db4d061d4ca7bc1db029881082af4081',
                container: document.querySelector('#form-address'),
                templates: {
                    value: function(suggestion) {
                        return suggestion.name;
                    }
                }
            }).configure({
                type: 'address'
            });
            placesAutocomplete.on('change', function resultSelected(e) {

                document.querySelector('#form-city').value = e.suggestion.city ;
                document.querySelector('#form-zip').value = e.suggestion.postcode ;
                var coord = e.suggestion.latlng ;
                if(coord){
                    document.querySelector('#latitude').value = coord.lat ;
                    document.querySelector('#longitude').value = coord.lng ;

                }

            });
        })();

        function previewFile(input){
            var file = $("input[type=file]").get(0).files[0];
            if(file){
                var reader = new FileReader();

                reader.onload = function(){
                    $("#previewImg").attr("src", reader.result);
                }

                reader.readAsDataURL(file);
            }
        }

        var textarea = document.querySelector('textarea');

        textarea.addEventListener('keydown', autosize);

        function autosize(){
            var el = this;
            setTimeout(function(){
                el.style.cssText = 'height:auto; padding:0';
                // for box-sizing other than "content-box" use:
                // el.style.cssText = '-moz-box-sizing:content-box';
                el.style.cssText = 'height:' + el.scrollHeight + 'px';
            },0);
        }

    </script>

@endsection
