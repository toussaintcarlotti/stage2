@extends('base')

@section('contenu')
    <link rel="stylesheet" href="{{ url('css/productGrid.css') }}">
    @include('sideBar')

    <div class="content">
        <div class="grid" style="padding: 5%">
                @foreach($entites as $entite)
                    <a href="{{ url('entites/show/'.$entite->id) }}" class="btn">
                        <article>
                            @if($entite->images()->where('type','principal')->first())
                                <img src="{{ url($entite->images()->where('type','principal')->first()->lien) }}" alt="Card image cap" >
                            @endif
                            <div class="text">
                                <h3>{{ $entite->nom }}</h3>

                                Tel : 0{{ $entite->tel }}</br>
                                Email : {{ $entite->email }}</br>
                                Adresse : {{ $entite->ville }}@if($entite->adresse), {{ $entite->adresse }}@endif

                            </div>
                        </article>
                    </a>
                @endforeach
        </div>
    </div>
@endsection
