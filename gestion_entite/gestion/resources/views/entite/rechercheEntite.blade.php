@extends('base')

@section('contenu')
    <link rel="stylesheet" href="{{ url('css/productGrid.css') }}">

    <div class="sidebar">
        <form class="form " method="post" >
            @csrf
            <h3>Filtrer</h3>
            <input type="hidden" value="{{ $search }}" name="nom">
            <input type="search" @if(isset($_POST['ville'])) value="{{ $_POST['ville'] }}" @endif placeholder="Ville" class="form-control mt-2" name="ville">
            @if(isset($cats))
                <select class="form-control form-select mt-2" name="categorie" id="categorie">
                    @if(isset($_POST['categorie']))
                        @if($_POST['categorie'])
                            <option value="{{ $_POST['categorie'] }}">{{ $cats->find($_POST['categorie'])->nom }}</option>
                        @else
                            <option value="">Categorie</option>
                        @endif
                    @else
                        <option value="">Categorie</option>
                    @endif
                    @foreach($cats as $cat)
                        <option value="{{$cat->id}}">{{$cat->nom}}</option>
                    @endforeach
                </select>
            @endif
            <div style="margin-bottom: 0px" class="group">
                <button class="btn btnBlack" style="width: 200px;" type="submit" >
                    Valider
                </button>
                <a class="btn btnBlack" href="{{ url("entites/index/") }}" style="width: 200px;margin: auto"  >
                    Reinitialiser
                </a>
            </div>
        </form>
    </div>

    <div class="content">
        <div class="grid" style="padding: 5%">
                @foreach($entites as $entite)
                    <a href="{{ url('entites/show/'.$entite->id) }}" class="btn">
                        <article>
                            @if($entite->images()->where('type','principal')->first())
                                <img src="{{ url($entite->images()->where('type','principal')->first()->lien) }}" alt="Card image cap" >
                            @endif
                            <div class="text">
                                <h3>{{ $entite->nom }}</h3>

                                Tel : 0{{ $entite->tel }}</br>
                                Email : {{ $entite->email }}</br>
                                Adresse : {{ $entite->ville }}@if($entite->adresse), {{ $entite->adresse }}@endif

                            </div>
                        </article>
                    </a>
                @endforeach
        </div>
    </div>

@endsection
