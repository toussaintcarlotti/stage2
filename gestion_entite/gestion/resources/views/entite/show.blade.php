@extends('base')

@section('contenu')

    <div class="container-fluid" style="max-width: 1350px">
        @if($entite->images()->where('type','principal')->first())
        <div class="d-flex justify-content-center" >
            <div class="col-md-7" style="min-width: 300px;min-height: 380px">
                <!-- #region Jssor Slider Begin -->
                <!-- Generator: Jssor Slider Composer -->
                <!-- Source: https://www.jssor.com/demos/full-width-slider.slider/=edit -->
                <script src="{{url('js/jssor.slider-28.0.0.min.js')}}" type="text/javascript"></script>
                <script src="{{url('js/slider.js')}}" type="text/javascript"> </script>
                <link href="//fonts.googleapis.com/css?family=Roboto+Condensed:300,300italic,regular,italic,700,700italic&subset=latin-ext,greek-ext,cyrillic-ext,greek,vietnamese,latin,cyrillic" rel="stylesheet" type="text/css" />
                <link rel="stylesheet" href="{{url('css/slider.css')}}">


                    <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:1200px;height:560px;overflow:hidden;visibility:hidden;">
                        <!-- Loading Screen -->
                        <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
                            <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="img/spin.svg" />
                        </div>
                        <div data-u="slides" style="object-fit: cover;cursor:default;position:relative;top:0px;left:0px;width:1200px;height:560px;overflow:hidden;">
                            @foreach($entite->images as $image)
                                <div >
                                    <img data-u="image" data-src="{{url($image->lien)}}" />
                                    <!--
                                    <div data-ts="flat" data-p="275" data-po="40% 50%" style="left:150px;top:40px;width:800px;height:300px;position:absolute;">
                                        <div data-to="50% 50%" data-t="0" style="left:50px;top:520px;width:400px;height:100px;position:absolute;color:#f0a329;font-family:'Roboto Condensed',sans-serif;font-size:84px;font-weight:900;letter-spacing:0.5em;">BEER</div>
                                        <div data-to="50% 50%" data-t="1" style="left:50px;top:540px;width:400px;height:100px;position:absolute;opacity:0.5;color:#f0a329;font-family:'Roboto Condensed',sans-serif;font-size:84px;font-weight:900;letter-spacing:0.5em;">BEER</div>
                                        <div data-to="50% 50%" data-t="2" style="left:50px;top:560px;width:400px;height:100px;position:absolute;opacity:0.25;color:#f0a329;font-family:'Roboto Condensed',sans-serif;font-size:84px;font-weight:900;letter-spacing:0.5em;">BEER</div>
                                        <div data-to="50% 50%" data-t="3" style="left:50px;top:580px;width:400px;height:100px;position:absolute;opacity:0.125;color:#f0a329;font-family:'Roboto Condensed',sans-serif;font-size:84px;font-weight:900;letter-spacing:0.5em;">BEER</div>
                                        <div data-to="50% 50%" data-t="4" style="left:50px;top:600px;width:400px;height:100px;position:absolute;opacity:0.06;color:#f0a329;font-family:'Roboto Condensed',sans-serif;font-size:84px;font-weight:900;letter-spacing:0.5em;">BEER</div>
                                        <div data-to="50% 50%" data-t="5" style="left:50px;top:710px;width:700px;height:100px;position:absolute;color:#f0a329;font-family:'Roboto Condensed',sans-serif;font-size:84px;font-weight:900;letter-spacing:0.5em;">MACHINE</div>
                                    </div>
                                    -->
                                </div>
                            @endforeach
                        </div><a data-scale="0" href="https://www.jssor.com" style="display:none;position:absolute;">slider html</a>
                        <!-- Bullet Navigator -->
                        <div data-u="navigator" class="jssorb132" style="position:absolute;bottom:24px;right:16px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
                            <div data-u="prototype" class="i" style="width:12px;height:12px;">
                                <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                                    <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                                </svg>
                            </div>
                        </div>
                        <!-- Arrow Navigator -->
                        <div data-u="arrowleft" class="jssora051" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
                            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                                <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
                            </svg>
                        </div>
                        <div data-u="arrowright" class="jssora051" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
                            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                                <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
                            </svg>
                        </div>
                    </div>
                    <script type="text/javascript">
                        jssor_1_slider_init();
                    </script>

            </div>
        </div>
        @endif
        <div class="row d-flex">
            <div class="col-md-12">
                <div style="text-align: justify">
                    <h1 style="text-align: center">{{ $entite->nom }}</h1>
                    {{ $entite->description }}
                </div>
            </div>
        </div>
        <div class="row d-flex justify-content-around" style="margin-top: 5% ">
            <div class="col-md-3 p-2" style="min-width: 300px">
                <div class="boite mb-4">
                    <h5 style="text-align: center">Coordonnée</h5>
                    <div class="card-body">
                        Email : {{ $entite->email }}<br/>
                        Numero : 0{{ $entite->tel }}
                    </div>
                </div >
            </div>

            @if($entite->horaires->first())
                <div class="col-md-4 p-2" style="min-width: 300px;text-align: center">
                    <div class="boite mb-4">
                        <h5 style="text-align: center">Horaire</h5>
                        <div class="card-body">
                            <?php
                            $tab = ['lundi','mardi','mercredi','jeudi','vendredi','samedi','dimanche'];
                            foreach ($tab as $jour){

                                ?>
                                @if(isset($entite->horaires()->where('days',$jour)->first()->h1))
                                     {{ $jour }} : {{ substr($entite->horaires()->where('days',$jour)->first()->h1,0,-3) }}
                                     à
                                    {{ substr($entite->horaires()->where('days',$jour)->first()->h2,0,-3) }}
                                    @if(isset($entite->horaires()->where('days',$jour)->first()->h3))
                                        et
                                       {{ substr($entite->horaires()->where('days',$jour)->first()->h3,0,-3) }}
                                        à
                                        {{ substr($entite->horaires()->where('days',$jour)->first()->h4,0,-3) }}

                                    @endif
                                    <br/>
                                @endif

                                <?php
                                }
                           ?>

                        </div>
                    </div>
                </div>
            @endif
            @if($entite->ville || $entite->adresse || $entite->codePostal)
                <div class="col-md-3 p-2" style="min-width: 300px;">
                    <div class="boite mb-4">
                        <h5 style="text-align: center">Localisation</h5>
                        <div class="card-body">
                            @if($entite->ville)Ville : {{ $entite->ville }}<br/> @endif
                            @if($entite->codePostal) Code postal : {{ $entite->codePostal }}<br/> @endif
                            @if($entite->adresse) Adresse : {{ $entite->adresse }} @endif
                        </div>
                    </div >
                </div>
            @endif
        </div>

    </div>


@endsection
