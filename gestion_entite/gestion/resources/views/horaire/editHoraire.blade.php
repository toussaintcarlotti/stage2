@extends('base')

@section('contenu')



    <div class="container table-responsive-sm" >
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{url('horaires/edit/'.$entite->id)}}" method="post" >
            @csrf
            <?php
            $tab2 = ['lundi','mardi','mercredi','jeudi','vendredi','samedi','dimanche'];
            foreach ($tab2 as $jour){
            ?>
            <div class="form-inline mt-3">

                <input type="hidden" name="{{ $jour }}" value="{{ $jour }}">
                <label style="min-width: 80px;text-transform: capitalize;">{{ $jour }} :</label>
                <div>

                    <input  @if(isset($entite->horaires()->where('days',$jour)->first()->h1)) value="{{ $entite->horaires()->where('days',$jour)->first()->h1 }}" @endif name="{{ $jour.'1' }}" id="{{ $jour.'1' }}" type="text" class="flatpickrTime form-control" >
                     à
                    <input  @if(isset($entite->horaires()->where('days',$jour)->first()->h2)) value="{{ $entite->horaires()->where('days',$jour)->first()->h2 }}" @endif name="{{ $jour.'2' }}" id="{{ $jour.'2' }}" type="text" class="flatpickrTime form-control">
                </div>
                <div id="{{ $jour.'e' }}" style="display: none">
                     et
                    <input @if(isset($entite->horaires()->where('days',$jour)->first()->h3)) value="{{ $entite->horaires()->where('days',$jour)->first()->h3 }}" @endif name="{{ $jour.'3' }}" id="{{ $jour.'3' }}" type="text" class="flatpickrTime form-control" >
                     à
                    <input @if(isset($entite->horaires()->where('days',$jour)->first()->h4)) value="{{ $entite->horaires()->where('days',$jour)->first()->h4 }}" @endif name="{{ $jour.'4' }}" id="{{ $jour.'4' }}" type="text" class="flatpickrTime form-control" >
                </div>
                <button class="btn " id="{{ "btn1".$jour }}" style="display: block;background: transparent" type="button" onclick="affiche({{ $jour.'e' }},{{ 'btn1'.$jour }},{{ 'btn2'.$jour }})">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-bar-right" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M6 8a.5.5 0 0 0 .5.5h5.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3a.5.5 0 0 0 0-.708l-3-3a.5.5 0 0 0-.708.708L12.293 7.5H6.5A.5.5 0 0 0 6 8zm-2.5 7a.5.5 0 0 1-.5-.5v-13a.5.5 0 0 1 1 0v13a.5.5 0 0 1-.5.5z"/>
                    </svg>
                </button>
                <button class="btn " id="{{ "btn2".$jour }}" style="display: none;background: transparent"  type="button" onclick="reduire({{ $jour.'e' }},{{ 'btn1'.$jour }},{{ 'btn2'.$jour }})">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-bar-left" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M12.5 15a.5.5 0 0 1-.5-.5v-13a.5.5 0 0 1 1 0v13a.5.5 0 0 1-.5.5zM10 8a.5.5 0 0 1-.5.5H3.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L3.707 7.5H9.5a.5.5 0 0 1 .5.5z"/>
                    </svg>
                </button>
            </div>
            <?php
            }
            ?>


            <button type="submit" class="btn btn-primary">valider</button>
        </form>
        <form action="{{ url("horaires/delete/".$entite->id) }}" method="POST" style="margin-right: 3px">
            @csrf
            @method("delete")
            <button class="btn btn-danger" type="submit" >
                delete
            </button>
        </form>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.2.3/flatpickr.js"></script>

    <script type="text/javascript" >
        $(".flatpickrTime").flatpickr({
            enableTime: true,
            noCalendar: true,
            time_24hr: true,
            dateFormat: "H:i",

        });
        function affiche(id,btn1,btn2) {
            id.style.display = "block";
            btn1.style.display = "none";
            btn2.style.display = "block";

        }
        function reduire(id,btn1,btn2) {
            id.style.display = "none";
            btn1.style.display = "block";
            btn2.style.display = "none";


        }
    </script>


@endsection
