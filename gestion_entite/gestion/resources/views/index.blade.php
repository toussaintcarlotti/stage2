@extends('base')

@section('contenu')
  <!--  <script src="{{url('js/jssor.slider-28.0.0.min.js')}}" type="text/javascript"></script>
    <script src="{{url('js/slider.js')}}" type="text/javascript"> </script>
    <link href="//fonts.googleapis.com/css?family=Roboto+Condensed:300,300italic,regular,italic,700,700italic&subset=latin-ext,greek-ext,cyrillic-ext,greek,vietnamese,latin,cyrillic" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{url('css/slider.css')}}">

        <div id="jssor_1" style="position:absolute;margin:0 auto;top:0px;left:0px;width:1200px;object-fit: cover;height:350px;overflow:hidden;visibility:hidden;">

            <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;object-fit: cover;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
                <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="img/spin.svg" />
            </div>
            <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1200px;height:350px;object-fit: cover;overflow:hidden;">
                @foreach($entites as $entite)
                    @if($entite->images()->first())
                        <div>
                        <img src="{{ url($entite->images()->inRandomOrder()->first()->lien) }}" style=";object-fit: cover;position: absolute;width: 100%;height: 400px;top:0px">
                        </div>
                    @endif
                @endforeach
            </div>
        <script type="text/javascript">
            jssor_1_slider_init();
        </script>
                </div>

-->
    <link rel="stylesheet" href="{{ url('css/imageCat.css') }}">
   <div class="container" style="margin-bottom: 50px">
       <div >
           <div class="row justify-content-md-center" >
               <div class="column" >


                       <a href="{{ url('entites/indexCat/restaurant') }}" class="hover10" >
                           <img class="content-image" src="{{ url('image/restaurant.jpg') }}" width="100px" style="height: 70px;object-fit: cover">
                           <div class="content-details fadeIn-bottom">
                               <h3 class="content-title">Restaurant</h3>
                           </div>
                       </a>

                   <a href="{{ url('entites/indexCat/magasin') }}" class="hover10"><img src="{{ url('image/magasin.jpg') }}" width="100px" style="height: 70px;object-fit: cover"></a>
                   <a href="{{ url('entites/indexCat/hotel') }}" class="hover10"> <img src="{{ url('image/hotel.jpg') }}" width="100px" style="height: 70px;object-fit: cover"></a>
                   <a href="{{ url('entites/indexCat/bar') }}" class="hover10"> <img src="{{ url('image/bar.jpg') }}" width="100px" style="height: 70px;object-fit: cover"></a>
                   <a href="{{ url('entites/indexCat/garage') }}"  class="hover10"> <img src="{{ url('image/garage.jpg') }}" width="100px" style="height: 70px;object-fit: cover" ></a>
                   <a href="{{ url('entites/indexCat/fleuriste') }}" class="hover10"><img src="{{ url('image/fleuriste.jpg') }}" width="100px" style="height: 70px;object-fit: cover"></a>
               </div>

           </div>
       </div>

   </div>


@endsection
