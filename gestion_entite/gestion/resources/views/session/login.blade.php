@extends("base")

@section("contenu")


    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-4" >
                <div class="card" style="border-radius: 10px">
                    <header class="card-header" style="border-top-left-radius: 10px;border-top-right-radius: 10px">
                        <a href="{{url('registration')}}" class="float-right btn btn-outline-secondary mt-1">S'enregistrer</a>
                        <h4 class="card-title mt-2">Connection</h4>
                    </header>
                    <article class="card-body" style="padding-left: 15%;padding-right: 15%">
                        <form action="{{ url('login') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label>Email </label>
                                <input type="email" class="form-control" id="email" name="email" autocomplete="email">
                            </div>
                            <div class="form-row">
                                <div class="col form-group">
                                    <label>Mot de passe</label>
                                    <input type="password" class="form-control" id="password" name="password">
                                </div>
                            </div>

                            <div class="form-group" style="text-align: center">
                                <button type="submit" class="btn btn-primary "> Log in </button>
                            </div>
                        </form>
                    </article>

                </div>
            </div>
        </div>
    </div>
@endsection
