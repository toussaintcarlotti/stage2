

<link rel="stylesheet" href="{{ url('css/productGrid.css') }}">
<div class="sidebar">
    <form class="form " >
        <h3>Filtrer</h3>
        <input type="search" @if(isset($_GET['ville'])) value="{{ $_GET['ville'] }}" @endif placeholder="Ville" class="form-control mt-2" name="ville">
        @if(isset($cats))
            <select class="form-control form-select mt-2" name="categorie" id="categorie">
                @if(isset($_GET['categorie']))
                    @if($_GET['categorie'])
                        <option value="{{ $_GET['categorie'] }}">{{ $cats->find($_GET['categorie'])->nom }}</option>
                    @else
                        <option value="">Categorie</option>
                    @endif
                @else
                    <option value="">Categorie</option>
                @endif
                @foreach($cats as $cat)
                    <option value="{{$cat->id}}">{{$cat->nom}}</option>
                @endforeach
            </select>
        @endif
        <div style="margin-bottom: 0px" class="group">
            <button class="btn btnBlack" style="width: 200px;" type="submit" >
                Valider
            </button>
            <a class="btn btnBlack"  href="{{ url("entites/index") }}" style="width: 200px;margin: auto"  >
                Reinitialiser
            </a>
        </div>
    </form>
</div>



</div>




