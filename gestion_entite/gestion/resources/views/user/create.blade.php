@extends('base')

@section('contenu')


    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-5" >
                <div class="card" style="border-radius: 10px">
                    <header class="card-header">
                        <a href="{{ url('login') }}" class="float-right btn btn-outline-secondary mt-1">Log in</a>
                        <h4 class="card-title mt-2">Sign up</h4>
                    </header>
                    <article class="card-body" style="border-top-left-radius: 10px;border-top-right-radius: 10px" >
                        <form method="post" action="{{ url('registration') }}">
                            @csrf
                            <div class="form-row">
                                <div class="col form-group">
                                    <label>Nom </label>
                                    <input type="text"  class="form-control" id="nom" name="nom" autocomplete="family-name">

                                </div> <!-- form-group end.// -->
                                <div class="col form-group">
                                    <label>Prenom</label>
                                    <input type="text" class="form-control" id="prenom" name="prenom" autocomplete="given-name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Email </label>
                                <input type="email" class="form-control" id="email" name="email" autocomplete="email">
                            </div>
                            <div class="form-group">
                                <label>Numero </label>
                                <input type="number" class="form-control" id="numero" name="numero" autocomplete="tel">
                            </div>
                            <div class="form-group">
                                <label class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="civilite" value="homme">
                                    <span class="form-check-label"> Homme </span>
                                </label>
                                <label class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="civilite" value="femme">
                                    <span class="form-check-label"> Femme </span>
                                </label>
                            </div>
                            <div class="form-row">
                                <div class="col form-group">
                                    <label>Mot de passe</label>
                                    <input type="password" class="form-control" id="password" name="password">
                                </div>
                                <div class="col form-group">
                                    <label for="password_confirmation">Confirmation mot de passe</label>
                                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
                                </div>
                            </div>

                            <div class="form-group" style="text-align: center">
                                <button type="submit" class="btn btn-primary" > S'enregistrer  </button>
                            </div>
                        </form>
                    </article>

                </div>
            </div>

        </div>


    </div>



@endsection
