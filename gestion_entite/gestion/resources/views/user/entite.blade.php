@extends('base')

@section('contenu')
<style>
    table td {
        cursor: pointer;
    }

</style>
<div class="container table-responsive-sm">
    @if(auth()->check())
    <table class="table">
        <tbody>
        @foreach(auth()->user()->entites as $entite)
        <tr >
            <td href="{{ url('entites/show/'.$entite->id) }}">@if($entite->images()->where('type','principal')->first())
                    <img style=" max-width: 100%;
    height: auto;max-height: 50px;border-radius: 5px" src="{{ url($entite->images()->where('type','principal')->first()->lien) }}" alt="Card image cap" >
                @else
                    <svg xmlns="http://www.w3.org/2000/svg" width="50"  fill="grey" class="bi bi-building" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M14.763.075A.5.5 0 0 1 15 .5v15a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5V14h-1v1.5a.5.5 0 0 1-.5.5h-9a.5.5 0 0 1-.5-.5V10a.5.5 0 0 1 .342-.474L6 7.64V4.5a.5.5 0 0 1 .276-.447l8-4a.5.5 0 0 1 .487.022zM6 8.694L1 10.36V15h5V8.694zM7 15h2v-1.5a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 .5.5V15h2V1.309l-7 3.5V15z"/>
                        <path d="M2 11h1v1H2v-1zm2 0h1v1H4v-1zm-2 2h1v1H2v-1zm2 0h1v1H4v-1zm4-4h1v1H8V9zm2 0h1v1h-1V9zm-2 2h1v1H8v-1zm2 0h1v1h-1v-1zm2-2h1v1h-1V9zm0 2h1v1h-1v-1zM8 7h1v1H8V7zm2 0h1v1h-1V7zm2 0h1v1h-1V7zM8 5h1v1H8V5zm2 0h1v1h-1V5zm2 0h1v1h-1V5zm0-2h1v1h-1V3z"/>
                    </svg>
                @endif</td>
            <td href="{{ url('entites/show/'.$entite->id) }}" style="font-weight: bold" scope="row">{{ $entite->nom }}</td>
            <td href="{{ url('entites/show/'.$entite->id) }}">{{ $entite->email }}</td>
            <td href="{{ url('entites/show/'.$entite->id) }}">0{{ $entite->tel }}</td>
            <th class="form-inline" >
                <a class="btn btn-warning" href="{{ url("entites/edit/".$entite->id) }}"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
                        <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                    </svg></a>
            </th>
        </tr>
        @endforeach
        </tbody>
    </table>
    @endif
    <div style="text-align: center">
        <a class="btn btn-primary mb-5" href="{{ url('entites/create') }}">Nouvelle Entite</a>
    </div>

</div>
<script>
    $(document).ready(function(){
        $('table td').click(function(){
            window.location = $(this).attr('href');
            return false;
        });
    });
</script>
@endsection
