<?php

use App\Http\Controllers\UploadController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'PageController@index');
//index entite
Route::get('/entites/index', 'EntiteController@index');
//recherche entite
Route::post('/entites/recherche', 'RechercheController@index');
//entite par categorie
Route::get('/entites/indexCat/{categorie}', 'CategorieController@index');
//show entite
Route::get('entites/show/{id}', 'EntiteController@show');


//dropzone
Route::get('dropzone/index/{entite}', 'DropzoneController@index');
Route::post('dropzone/upload', 'DropzoneController@upload')->name('dropzone.upload');
Route::get('dropzone/fetch', 'DropzoneController@fetch')->name('dropzone.fetch');
Route::get('dropzone/delete', 'DropzoneController@delete')->name('dropzone.delete');

Route::middleware('auth')->group(function () {
    //----User controller----
    //edit user
    Route::get('user/edit', 'UserController@edit');
    Route::post('user/edit', 'UserController@update');
    //----Session controller----
    //destroy session
    Route::post('logout', 'SessionUserController@destroy');

    //----EntiteUser controller----
    //index entite user
    Route::get('user/{id}/entites', 'EntiteUserController@index');

    //----Entite controller----
    //create entite
    Route::get('entites/create', 'EntiteController@create');
    Route::post('entites/create', 'EntiteController@store');

    //edit entite
    Route::get('entites/edit/{entite}', 'EntiteController@edit');
    Route::post('entites/edit/{entite}', 'EntiteController@update');
    //destroy enite
    Route::delete('entites/delete/{entite}', 'EntiteController@destroy');

    //----Horaire controller
    //create horaire
    Route::get('horaires/create/{entite}', 'HoraireController@create');
    Route::post('horaires/create/{entite}', 'HoraireController@store');
    //edit horaire
    Route::get('horaires/edit/{entite}', 'HoraireController@edit');
    Route::post('horaires/edit/{entite}', 'HoraireController@update');
    //delete horaire
    Route::delete('horaires/delete/{entite}', 'HoraireController@destroy');

});


Route::middleware('guest')->group(function () {
    //----Session controller----
    //create
    Route::get('login', 'SessionUserController@create');
    Route::post('login', 'SessionUserController@store');
    //----User controller----
    //create
    Route::get('registration', 'UserController@create');
    Route::post('registration', 'UserController@store');
});
