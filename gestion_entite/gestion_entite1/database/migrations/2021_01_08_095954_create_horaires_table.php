<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHorairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horaires', function (Blueprint $table) {
            $table->id();
            $table->string("lundi")->nullable();
            $table->string("mardi")->nullable();
            $table->string("mercredi")->nullable();
            $table->string("jeudi")->nullable();
            $table->string("vendredi")->nullable();
            $table->string("samedi")->nullable();
            $table->string("dimanche")->nullable();
            $table->unsignedBigInteger("entite_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horaires');
    }
}
