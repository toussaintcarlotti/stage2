<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class EditUserController extends Controller
{
    public function create()
    {
        return view('auth.edit');

    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update($id)
    {
        $user = User::findOrFail($id);
        $email = $user->email;

        $attribute = request()->validate([
            'nom' => 'required|string|max:255',
            'prenom' => 'required|string',
            'civilite' => 'required|string',
            'numero' => 'required|numeric',
            'password' => 'required|string|min:8'
        ]);

        if ($email != request('email')){
            $attribute = request()->validate([
                'email' => 'required|string|email|max:255|unique:users'
            ]);
        }

        $attribute['password'] = Hash::make($attribute['password']);
        $user->update($attribute);


        return redirect(RouteServiceProvider::HOME);
    }
}
