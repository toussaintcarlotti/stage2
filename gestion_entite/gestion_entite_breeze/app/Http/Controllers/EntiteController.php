<?php

namespace App\Http\Controllers;

use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;

class EntiteController extends Controller
{
    public function create()
    {
        return redirect(RouteServiceProvider::HOME);
    }
}
