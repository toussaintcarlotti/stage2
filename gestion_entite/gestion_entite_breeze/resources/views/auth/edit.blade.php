<x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </x-slot>

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ url("edit/".Auth::user()->id) }}">
        @csrf

        <!-- Nom -->
            <div>
                <x-label for="nom" :value="__('Nom')" />

                <x-input id="nom" autocomplete="family-name" class="block mt-1 w-full" type="text" name="nom" value="{{Auth::user()->nom}}" required  />
            </div>

            <!-- Prenom -->
            <div>
                <x-label for="prenom" :value="__('Prenom')" />

                <x-input id="prenom" autocomplete="given-name" class="block mt-1 w-full" type="text" name="prenom" value="{{Auth::user()->prenom}}" required autofocus />
            </div>


            <!-- Civilite -->
            <div>
                <x-label for="civilite" :value="__('Civilite')" />

                <x-input id="civilite" autocomplete="honorific-prefix" class="block mt-1 w-full" type="text" name="civilite" value="{{Auth::user()->civilite}}" required autofocus />
            </div>


            <!-- Numero -->
            <div>
                <x-label for="numero" :value="__('Numero')" />

                <x-input id="numero" autocomplete="tel" class="block mt-1 w-full" type="number" name="numero" value="{{Auth::user()->numero}}" required autofocus />
            </div>

            <!-- Email Address -->
            <div class="mt-4">
                <x-label for="email" :value="__('Email')" />

                <x-input id="email" autocomplete="email" class="block mt-1 w-full" type="email" name="email" value="{{Auth::user()->email}}" required />
            </div>

            <!-- Password -->
            <div class="mt-4">
                <x-label for="password" :value="__('Mot de passe')" />

                <x-input id="password" class="block mt-1 w-full"
                         type="password"
                         name="password"
                         required autocomplete="new-password" />
            </div>

            <!-- Confirm Password -->
            <div class="mt-4">
                <x-label for="password_confirmation" :value="__('Confirmer mot de passe')" />

                <x-input id="password_confirmation" class="block mt-1 w-full"
                         type="password"
                         name="password_confirmation" required />
            </div>

            <div class="flex items-center justify-end mt-4">


                <x-button class="ml-4">
                    {{ __('Modifier') }}
                </x-button>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout>
