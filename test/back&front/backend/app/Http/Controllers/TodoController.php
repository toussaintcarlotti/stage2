<?php

namespace App\Http\Controllers;

use App\Models\Todo;
use Illuminate\Http\Request;

class TodoController extends Controller
{
    public function index()
    {
        $todos = Todo::get();
        return $todos;

    }
    public function store()
    {
        $item = request()->validate([
            'text' => 'required',
            'checked' => 'required'
        ]);
        Todo::create($item);
    }

    public function destroy(){
        $todosId = request()->validate([
            'id' => 'required'
        ]);
        foreach ($todosId as $itemId){
            Todo::find($itemId)->delete();

        }

    }
}
