<?php

namespace App\Http\Controllers;

use App\Models\Categorie;


class CategorieController extends Controller
{

    public function create()
    {
        $cats = Categorie::get();
        return view("categorie", compact("cats"));
    }


    public function store()
    {
        $attribute = request()->validate([
            "nom" => "required|min:3"
        ]);
        $categorie = Categorie::create($attribute);
        return redirect("/index");
    }


    public function destroy(Categorie $categorie)
    {

        $categorie->delete();
        return redirect("/index");
    }
}
