<?php

namespace App\Http\Controllers;

use App\Models\Categorie;
use App\Models\Pointvente;


class PointventeController extends Controller
{

    public function create()
    {
            $points = Pointvente::get();
            return view("pointvente",compact("points"));
    }
    public function store()
    {
        $attribute = request()->validate([
            "nom" => "required|min:3"
        ]);
        $pointvente = Pointvente::create($attribute);
        return redirect("/index");
    }


    public function destroy(Pointvente $pointvente)
    {

        $pointvente->delete();
        return redirect("/index");
    }
}
