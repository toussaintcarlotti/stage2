<?php

namespace App\Http\Controllers;

use App\Models\Categorie;
use App\Models\Pointvente;
use App\Models\Produit;
use App\Models\Category;
use phpDocumentor\Reflection\Types\This;

class ProduitController extends Controller
{
    public function index(){

        $produits = new Produit;
        if (request('id') ){
            $produits = $produits->where('id',request('id'));
        }
        if (request('nom')) {
            $produits = $produits->where('nom','like',request('nom').'%');
        }
        if (request('lieu')) {
            $produits = $produits->where('lieuFabrication',request('lieu'));
        }
        if (request('date')) {
            $produits = $produits->where('dateFabrication',request('date'));
        }

        if (request('prixmin') && request('prixmax')) {
            $produits = $produits->whereBetween('prix',[request('prixmin'),request('prixmax')]);
        }else{
            if (request('prixmin')) {
                $produits = $produits->where('prix',">=",request('prixmin'));
            }
            if (request('prixmax')){
                $produits = $produits->where('prix',"<=",request('prixmax'));
            }
        }
        if (request('categorie') ){
            $produits = $produits->where("categorie_id", request('categorie'));
        }
        $produits = $produits->simplePaginate(5);
        $cats = Categorie::all();

        return view("index", compact("produits","cats"));
    }


    public function create(){
        $cats = Categorie::get();
        $points = Categorie::get();
        return view("Produit",compact("cats","points"));



    }
    public function store(){

        $attribute = request()->validate([
            "nom" => "required|min:3",
            "dateFabrication" => "required",
            "lieuFabrication" => "required",
            "prix" => "required",
            "categorie_id"=> "sometimes"
        ]);

        $produit = Produit::create($attribute);
        if (request("pointvente_id")){
            $point = Pointvente::findOrFail(request("pointvente_id"));
            $produit->pointvente()->sync($point);
        }

        return redirect("/index");

    }
   public function edit(){
        $produit = Produit::findOrFail(request('id'));
        $points = Pointvente::get();
        $cats = Categorie::get();
        return view("edit",compact("produit","cats","points"));
   }
    public function update(){

        $attribute = request()->validate([
            "nom" => "required|min:3",
            "dateFabrication" => "required",
            "lieuFabrication" => "required",
            "prix" => "required",
            "categorie_id" =>"sometimes"

        ]);
        $produit = Produit::findOrFail(request('id'));


        if (request("pointvente_id")){
            $point = Pointvente::findOrFail(request("pointvente_id"));
            $produit->pointvente()->sync($point);
        }

        $produit->update($attribute);


        return redirect("/index");
    }
    public function destroy(Produit $produit){

        $produit->delete();
        return redirect("/index");
    }
}
