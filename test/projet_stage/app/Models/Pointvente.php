<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pointvente extends Model
{
    use HasFactory;
    public $fillable = ["nom"];
    public function produit()
    {
        return $this->hasMany(Produit::class);
    }
}
