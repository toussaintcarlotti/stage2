<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produit extends Model
{
    use HasFactory;
    public $fillable = ["nom","dateFabrication","lieuFabrication","prix","categorie_id"];
    public $dates = ["dateFabrication"];

    public function categorie()
    {
        return $this->belongsTo(Categorie::class);
    }
    public function pointvente()
    {
        return $this->belongsToMany(Pointvente::class);
    }

}
