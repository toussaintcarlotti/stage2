<?php

namespace Database\Factories;

use App\Models\Pointvente;
use Illuminate\Database\Eloquent\Factories\Factory;

class PointventeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Pointvente::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "nom"=> $this->faker->word
        ];
    }
}
