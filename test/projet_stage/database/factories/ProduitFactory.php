<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Produit;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProduitFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Produit::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "nom"=> $this->faker->word,
            "dateFabrication"=> $this->faker->date(),
            "lieuFabrication"=> $this->faker->city,
            "prix"=> $this->faker->numberBetween(0,500),
            "categorie_id"=> $this->faker->numberBetween($min = 1, $max = 5)

        ];
    }
}
