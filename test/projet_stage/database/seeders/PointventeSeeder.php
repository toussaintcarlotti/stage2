<?php

namespace Database\Seeders;

use App\Models\Pointvente;
use App\Models\Produit;
use Illuminate\Database\Seeder;

class PointventeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pointvente::factory()
            ->times(10)
            ->create();

    }
}
