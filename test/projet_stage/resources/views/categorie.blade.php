@extends('template')

@section('contenu')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ url('/cat') }}" method="POST" >
        @csrf
        <div class="form-group">
            <div class="form-inline"><label for="nom">Entrez le nom de la catégorie : </label>
                <input class="form-control" type="text" name="nom" id="nom">
                <button class="btn btn-primary" type="submit" >ajouter</button>
            </div>
        </div>
    </form>


        <table class="table table-striped table-bordered" style="margin-top: 10px;width: 500px;float: right;margin-top: -50px">
            <tr>
                <th>Categories</th>
            </tr>
            @foreach($cats as $cat)
                <tr>
                    <td>{{ $cat->nom }}
                        <form action="{{ url('categories/'.$cat->id) }}" method="POST">
                            @csrf
                            @method("delete")
                            <button class="btn btn-danger" type="submit" >Supprimer</button>
                        </form> </td>
                </tr>
            @endforeach
        </table>


@endsection
