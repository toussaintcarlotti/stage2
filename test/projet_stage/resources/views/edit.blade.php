@extends('template')

@section('contenu')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form class="row g-3" method="POST" action="{{url("edit?id=".$produit->id)}}">
        @csrf

        <div class="col-auto">
        <div class="form-inline">
            <label for="nom">Entrez le nom du produit : </label>
            <input class="form-control" type="text" name="nom" id="nom" value="{{$produit->nom}}">
        </div>
        <div class="form-inline"> <label for="dateFabrication">Entrez la date de fabrication du produit : </label>
            <input class="form-control" type="date" name="dateFabrication" id="dateFabrication" value="{{$produit->dateFabrication->format('Y-m-d')}}">
        </div>
        <div class="form-inline"><label for="lieuFabrication">Entrez le lieu de fabrication du produit :</label>
            <input class="form-control" type="text" name="lieuFabrication" id="lieuFabrication" value="{{$produit->lieuFabrication}}">
        </div>
        <div class="form-inline"><label for="prix">Entrez le prix : </label>
            <input class="form-control" type="number" name="prix" id="prix" value="{{$produit->prix}}">
        </div>
        <div class="form-inline"><label for="categorie">Choisissez une categorie : </label>
            <select class="form-control" name="categorie_id" id="categorie">
                @if($produit->categorie)
                <option value="{{ $produit->categorie_id }}">{{ $produit->categorie->nom }}</option>
                @endif
                <option value="">Aucune une categorie</option>

                @foreach($cats as $cat)
                    <option value="{{$cat->id}}">{{$cat->nom}}</option>
                @endforeach

            </select>
        </div>
        </div>
        <div class="col-auto">
            <table class="table table-striped table-bordered" >
                <tr>
                    <th>Point de vente</th>
                </tr>
                @foreach($points as $point)
                    <tr>
                        <td>{{ $point->nom }}
                        <input name="pointvente_id[]" value="{{ $point->id }}" type="checkbox" @foreach($produit->pointvente as $pointz) @if($point->id == $pointz->id) checked @endif @endforeach>
                        </td>

                    </tr>
                @endforeach
            </table>

        <input class="btn btn-primary" type="submit" value="modifier">
        </div>

    </form>



@endsection
