@extends("template")

@section("contenu")


    <form class="row g-3">

        <div class="col-auto">
            <input class="form-control" type="number" placeholder="min" @if(isset($_GET['prixmin']))value="{{ $_GET['prixmin'] }}"@endif name="prixmin" id="prix" style="width: 100px">
        </div>
        <div class="col-auto">
            <input class="form-control" type="number" placeholder="max" @if(isset($_GET['prixmax']))value="{{ $_GET['prixmax'] }}"@endif name="prixmax" id="prix"  style="width: 100px">
        </div>
        <div class="col-auto">
           <input class="form-control" type="text" name="nom" @if(isset($_GET['nom']))value="{{ $_GET['nom'] }}"@endif placeholder="nom" style="width: 100px">
        </div>
        <div class="col-auto">
            <input class="form-control" type="number" name="id" @if(isset($_GET['id']))value="{{ $_GET['id'] }}"@endif id="id" placeholder="id" style="width: 100px">
        </div>
        <div class="col-auto">
            <input class="form-control" type="text" name="lieu" @if(isset($_GET['lieu']))value="{{ $_GET['lieu'] }}"@endif placeholder="lieu" style="width: 100px">
        </div>
        <div class="col-auto">
            <input class="form-control" type="date" name="date" @if(isset($_GET['date']))value="{{ $_GET['date'] }}"@endif placeholder="date" style="width: 150px">
        </div>

        <div class="col-auto">
            <select class="form-control form-select" name="categorie" id="categorie">
                @if(isset($_GET['categorie']))
                    @if($_GET['categorie'])
                        <option value="{{ $_GET['categorie'] }}">{{ $cats->find($_GET['categorie'])->nom }}</option>
                    @else
                        <option value="">Categorie</option>
                    @endif
                @else
                    <option value="">Categorie</option>
                @endif
                @foreach($cats as $cat)
                    <option value="{{$cat->id}}">{{$cat->nom}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-auto">
            <button class="btn btn-primary mb-3" type="submit" style="border:transparent;background: transparent;margin-right: 10px">
                <svg xmlns="http://www.w3.org/2000/svg" width="20"  fill="black" class="bi bi-search" viewBox="0 0 16 16">
                    <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                </svg>
            </button>
        </div>
        <div class="col-auto">
            <button class="btn btn-primary mb-3" type="submit" style="border:transparent;background: transparent;margin-right: 10px">
                <a  href="/" ><svg xmlns="http://www.w3.org/2000/svg" width="25"  fill="black" class="bi bi-x-circle-fill" viewBox="0 0 16 16">
                        <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM5.354 4.646a.5.5 0 1 0-.708.708L7.293 8l-2.647 2.646a.5.5 0 0 0 .708.708L8 8.707l2.646 2.647a.5.5 0 0 0 .708-.708L8.707 8l2.647-2.646a.5.5 0 0 0-.708-.708L8 7.293 5.354 4.646z"/>
                    </svg>
                </a>
            </button>
        </div>



    </form>

    <table class="table table-striped table-bordered" style="margin-top: 10px">
        <tr>
            <th>Id</th>
            <th>Nom</th>
            <th>Date de fabrication</th>
            <th>Lieu de fabrication</th>
            <th>Prix</th>
            <th>Categorie</th>
            <th>Point de vente</th>
            <th>Actions</th>
        </tr>
        @foreach($produits as $produit)
            <tr>
                <td>{{$produit->id}}</td>
                <td >{{$produit->nom}}</td>
                <td>{{$produit->dateFabrication}}</td>
                <td>{{$produit->lieuFabrication}}</td>
                <td>{{$produit->prix}}</td>
                <td>@if($produit->categorie) {{$produit->categorie->nom}} @endif</td>
                <td>@foreach($produit->pointvente as $point) <li>{{ $point->nom }}</li>  @endforeach</td>

                <td style="text-align: center;">

                    <a href="{{url("edit?id=".$produit->id)}}" class="btn btn-warning"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
                            <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                        </svg></a>

                    <form action="{{ url("produits/".$produit->id) }}" method="POST">
                        @csrf
                        @method("delete")
                        <input type="hidden" name="id"  value="{{ $produit->id }}">
                        <button class="btn btn-danger" type="submit" ><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill" viewBox="0 0 16 16">
                                <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
                            </svg></button>
                    </form>


                </td>
            </tr>
        @endforeach
    </table>
<div class="paginate">
    {!! $produits->appends(request()->query())->links() !!}
</div>




@endsection
