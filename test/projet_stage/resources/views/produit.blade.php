@extends('template')

@section('contenu')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ url('/') }}" method="POST" >
        @csrf
        <div class="form-group">
            <div class="form-inline"><label for="nom">Entrez le nom du produit : </label>
            <input class="form-control" type="text" name="nom" id="nom">
            </div>
            <div class="form-inline"> <label for="dateFabrication">Entrez la date de fabrication du produit : </label>
            <input class="form-control" type="date" name="dateFabrication" id="dateFabrication">
            </div>
            <div class="form-inline"><label for="lieuFabrication">Entrez le lieu de fabrication du produit :</label>
            <input class="form-control" type="text" name="lieuFabrication" id="lieuFabrication">
            </div>
            <div class="form-inline"><label for="prix">Entrez le prix : </label>
            <input class="form-control" type="number" name="prix" id="prix">
            </div>
            <div class="form-inline"><label for="categorie_id">Choisissez votre categorie : </label>
                <select class="form-control" name="categorie_id" id="categorie_id">
                    <option value="">Categorie</option>
                    @foreach($cats as $cat)
                        <option value="{{$cat->id}}">{{$cat->nom}}</option>
                    @endforeach

                </select>
            </div>
            <div class="form-inline">
            <table class="table table-striped table-bordered" style="top: 90px;width: 500px;right:200px;position: absolute">
                <tr>
                    <th>Point de vente</th>
                </tr>
                @foreach($points as $point)
                    <tr>
                        <td>{{ $point->nom }}
                            <input name="pointvente_id[]" value="{{ $point->id }}" type="checkbox" >
                        </td>

                    </tr>
                @endforeach
            </table>
            </div>
            <button class="btn btn-primary" type="submit" >ajouter</button>
        </div>
    </form>
@endsection
