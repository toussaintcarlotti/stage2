<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" type="text/css" href="{{ url('/css/style.css') }}" />

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
          rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <title>LaravelStage</title>


</head>
<body>


<nav class="navbar bg-dark text-white" >

    <div class="container-fluid">
        @if( auth()->check() )
            <li class="nav-item">
                <a class="nav-link font-weight-bold" href="#">Hi {{ auth()->user()->name }}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/logout">Log Out</a>
            </li>
        @else
            <li class="nav-item">
                <a class="nav-link" href="/login">Log In</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/register">Register</a>
            </li>
        @endif
        <ul class="nav"  style="width: 300px;margin-left: 15%;text-align: center">
            <li class="nav-item"><a  class="nav-link " href="/index" style="color: white">Menu</a></li>
        </ul>
        <ul id="menu-demo2" class="nav" >
            <li><a href="">Gerer</a>
                <ul>
                    <li class="nav-item"><a class="nav-link " href="{{url("createcat")}}" > Categorie</a></li>
                    <li class="nav-item"><a class="nav-link " href="{{url("createpoint")}}" > Point de vente</a></li>
                </ul>
            </li>

        </ul>
        <ul id="menu-demo2" class="nav" style="margin-right: -30%;margin-left: -10% ">
            <li><a href="">Ajout</a>
                <ul>
                    <li class="nav-item"><a class="nav-link " href="{{url("create")}}" > Produit</a></li>
                </ul>
            </li>

        </ul>

        <ul class="nav" style="width: 500px;margin-right: 5%">
          </ul>
    </div>
</nav>
<div class="container" style="margin-top: 10px">
@yield('contenu')
</div>
</body>

<script>
    jQuery( document ).ready( function($) {
        $( window ).scroll( function () {
            if ( $(document).scrollTop() > 150 ) {
                // Navigation Bar
                $('.navbar').removeClass('fadeIn');
                $('body').addClass('shrink');
                $('.navbar').addClass('animated fadeInDown');
            } else {
                $('.navbar').removeClass('fadeInDown');
                $('body').removeClass('shrink');
                $('.navbar').addClass('animated fadeIn');
            }
        });
    });
</script>

</html>
