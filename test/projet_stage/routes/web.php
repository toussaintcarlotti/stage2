<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware('auth')->group(function () {
    Route::get('/index', "ProduitController@index");

    Route::get('create', "ProduitController@create");
    Route::get('/createcat', "CategorieController@create");
    Route::get('/createpoint', "PointventeController@create");


    Route::post('/index', "ProduitController@store");
    Route::post('/cat', "CategorieController@store");
    Route::post('/point', "PointventeController@store");

    Route::delete('produits/{produit}', "ProduitController@destroy");
    Route::delete('categories/{categorie}', "CategorieController@destroy");
    Route::delete('pointventes/{pointvente}', "PointventeController@destroy");

    Route::get('edit', "ProduitController@edit");

    Route::post('edit', "ProduitController@update");

    Route::get('/logout', 'SessionsController@destroy');
});

Route::middleware('guest')->group(function () {
    Route::get('/register', 'RegistrationController@create');
    Route::post('register', 'RegistrationController@store');

    Route::get('/', 'SessionsController@create');
    Route::post('/', 'SessionsController@store');
});


