@extends("layout")

@section("contenu")



<!-- component -->
<div class="container bg-gray-100">


        <div class="grid min-h-screen place-items-center ">
            <div class="w-11/12 p-12 bg-white rounded sm:w-8/12 md:w-1/2 lg:w-5/12 ">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

               <form method="POST" action="{{ route('register') }}" class="mt-6 ">
                   @csrf
                <div class="flex justify-between gap-3 ">
                    <span class="w-1/2">
                      <label for="nom">Nom</label>
                      <input id="nom" type="text" name="nom" autocomplete="family-name" class="block rounded mt-1 w-full "  required />
                    </span>
                    <span class="w-1/2">
                      <label for="prenom" >Prenom</label>
                      <input id="prenom" type="text" name="prenom"  autocomplete="given-name" class="block rounded mt-1 w-full"  required />
                    </span>
                </div>
                   <label for="civilite" >civilite</label>
                   <input id="civilite" type="text" name="civilite"  autocomplete="honorific-prefix" class="block rounded mt-1 w-full"  required />
                   <label for="numero" >numero</label>
                   <input id="numero" type="number" name="numero"  autocomplete="tel" class="block rounded mt-1 w-full"  required />
                   <label for="email" >email</label>
                   <input id="email" type="text" name="email"  autocomplete="email" class="block rounded mt-1 w-full"  required />

                    <label for="password" class="block mt-2 text-xs font-semibold text-gray-600 uppercase">Password</label>
                    <input id="password" type="password" name="password" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner" required />

                   <label for="password_confirmation" class="block mt-2 text-xs font-semibold text-gray-600 uppercase">Confirm password</label>
                    <input id="password_confirmation" type="password" name="password_confirmation" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner" required />

                   <button type="submit" class="w-full py-3 mt-6 font-medium tracking-widest text-white uppercase bg-black shadow-lg focus:outline-none hover:bg-gray-900 hover:shadow-none">
                        S'enregistrer
                    </button>
                    <p class="flex justify-between inline-block mt-4 text-xs text-gray-500 cursor-pointer hover:text-black">Already registered?</p>
               </form>
            </div>
        </div>
</div>


@endsection
