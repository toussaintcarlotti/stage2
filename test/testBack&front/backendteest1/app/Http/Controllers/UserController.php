<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function store(){
        $attribute = request()->validate([
            'name'=>'required',
            'email'=> 'required|email',
            'password'=>'required'
        ]);
        User::create($attribute);
    }
}
